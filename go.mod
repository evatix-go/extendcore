module gitlab.com/evatix-go/extendcore

go 1.17

require (
	gitlab.com/evatix-go/core v1.3.55
	gitlab.com/evatix-go/enum v0.4.2
	gitlab.com/evatix-go/errorwrapper v1.1.5
)

require golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
